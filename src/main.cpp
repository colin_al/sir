#include <Book/Application.hpp>

#include <stdexcept>
#include <iostream>

#include "zmq.hpp"
#include "Debug.hpp"

void mainClient();
void mainServer();
void mainGame();

enum class Mode : int { GAME, CLIENT, SERVER };

int main() {
	try {
		Application app;
		app.run();
	}
	catch (std::exception& e) {
		std::cout << "\nEXCEPTION: " << e.what() << std::endl;
		system("pause");
	}
}