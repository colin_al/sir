#include "ZMQ_SFML.hpp"


bool send(zmq::socket_t& socket, sf::Packet& packet) {
	size_t size = packet.getDataSize();
	size_t sentSize = socket.send(packet.getData(), size);
	if (size != sentSize)
		return false;
	packet.clear();
	return true;
}

std::shared_ptr<sf::Packet> receive(zmq::socket_t& socket) {
	std::shared_ptr<sf::Packet> packet(new sf::Packet);
	zmq::message_t message;
	socket.recv(&message);
	message.data();
	packet->append(message.data<void>(), message.size());
	return packet;
}