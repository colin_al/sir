#pragma once



#include "zmq.hpp"
#include <memory>
#include <SFML/Network.hpp>

bool send(zmq::socket_t& socket, sf::Packet& packet);

std::shared_ptr<sf::Packet> receive(zmq::socket_t& socket);