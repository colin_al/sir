#include "NetworkConfiguration.hpp"


std::ostream& operator<< (std::ostream& os, NetworkConfiguration::Mode mode) {
	switch (mode) {
		case NetworkConfiguration::Mode::SFML_TCP: os << "SFML_TCP";	break;
		case NetworkConfiguration::Mode::REQ_REP: os << "REQ_REP";  break;
		case NetworkConfiguration::Mode::PUB_SUB: os << "PUB_SUB"; break;
		default: os << "Unknown NetMode"; break;
	}
	return os;
}

NetworkConfiguration::NetworkConfiguration() {
	{ // Try to open existing file (RAII block)
		std::ifstream inputFile("ip.txt");
		std::string ipAddress;
		char mode;
		if (inputFile >> mode && inputFile >> ipAddress && inputFile >> _port) {
			switch (mode) {
				case 'T': _mode = Mode::SFML_TCP; break;
				case 'R': _mode = Mode::REQ_REP; break;
				case 'P': _mode = Mode::PUB_SUB; break;
			}
			_ipAdress = ipAddress;
			return;
		}
	}
	// If open/read failed, create new file
	std::ofstream outputFile("ip.txt");
	outputFile << _ipAdress.toString() << std::endl << _port;
}