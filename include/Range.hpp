#pragma once

class range final {
public:
	class iterator final {
	public:
		inline iterator& operator ++ () noexcept { _value += _step; return *this; }
		inline bool operator != (const iterator& i) const noexcept { return _value != i._value; }
		inline int operator * () const noexcept { return _value; }
		iterator(int value, int step) noexcept : _value(value), _step(step) {}
	private:
		int _value, _step;
	};
	iterator begin() const noexcept { return iterator(_begin, _step); }
	iterator end() const noexcept { return iterator(_end, _step); }
	range(int begin, int end, int step = 1) noexcept :
	_begin(begin), _end(end - (end - begin) % step + step), _step(step) {}
private:
	int _begin, _end, _step;
};