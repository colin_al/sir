#pragma once

#include <iostream>

#define DEBUG(arg) std::cout << __FILE__ << " (" << __LINE__ << "), " <<  #arg << " : " << arg << std::endl
#define PRINT(arg) std::cout << __FILE__ << " (" << __LINE__ << "), " << arg << std::endl

#define CHECKPOINT std::cout << __FILE__ << " (" << __LINE__ << std::endl

#define NOT_IMPLEMENTED throw std::exception("NOT_IMPLEMENTED");

/*

switch (_config.getMode()) {
case NetworkConfiguration::Mode::SFML_TCP:
break;
case NetworkConfiguration::Mode::REQ_REP:
break;
case NetworkConfiguration::Mode::PUB_SUB:
break;
default:
break;
}


if (send(*zmqSocket, packet)) {
std::shared_ptr<sf::Packet> rep = receive(*zmqSocket);
sf::Int32 packetType;
*rep >> packetType;
// check packetType = Server::Ack
}

*/