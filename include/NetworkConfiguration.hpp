#pragma once

#include <SFML/Network/IpAddress.hpp>
#include <fstream>
#include "Debug.hpp"


class NetworkConfiguration {
public:
	enum class Mode{ SFML_TCP, REQ_REP, PUB_SUB };
	inline Mode getMode() const { return _mode; }
	inline const sf::IpAddress& getIp() const { return _ipAdress; }
	inline unsigned short getPort() const { return _port; }
	
	NetworkConfiguration();
private:
	Mode _mode;
	sf::IpAddress _ipAdress = sf::IpAddress("127.0.0.1");
	unsigned short _port = 5000;
};